/*
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * https://www.scm-manager.com
 *
 */

Triology.groupmanager.Grid = Ext.extend(Sonia.rest.Grid, {

  colNameText : Triology.groupmanager.I18n.colNameText,
  colDescriptionText : Triology.groupmanager.I18n.colDescriptionText,
  colMembersText : Triology.groupmanager.I18n.colMembersText,
  colGroupManagerText : Triology.groupmanager.I18n.colGroupManagerText,
  colCreationDateText : Triology.groupmanager.I18n.colCreationDateText,
  colTypeText : Triology.groupmanager.I18n.colTypeText,
  emptygroupManagerStoreText : Triology.groupmanager.I18n.emptygroupManagerStoreText,
  groupFormTitleText : Triology.groupmanager.I18n.groupFormTitleText,

  // parent panel for history
  parentPanel : null,

  initComponent : function() {
    var groupManagerStore = new Sonia.rest.JsonStore({
      proxy : new Ext.data.HttpProxy({
        url : restUrl + 'groupmanager.json',
        disableCaching : false
      }),
      idProperty : 'name',
      fields : [ 'name', 'members', 'groupmanager', 'description', 'creationDate', 'type', 'properties' ],
      sortInfo : {
        field : 'name'
      }
    });

    var groupColModel = new Ext.grid.ColumnModel({
      defaults : {
        sortable : true,
        scope : this,
        width : 125
      },
      columns : [ {
        id : 'name',
        header : this.colNameText,
        dataIndex : 'name'
      }, {
        id : 'description',
        header : this.colDescriptionText,
        dataIndex : 'description',
        width : 300
      }, {
        id : 'members',
        header : this.colMembersText,
        dataIndex : 'members',
        renderer : this.renderMembers
      }, {
        id : 'groupmanager',
        header : this.colGroupManagerText,
        dataIndex : 'groupmanager',
        renderer : this.renderMembers
      }, {
        id : 'creationDate',
        header : this.colCreationDateText,
        dataIndex : 'creationDate',
        renderer : Ext.util.Format.formatTimestamp
      }, {
        id : 'type',
        header : this.colTypeText,
        dataIndex : 'type',
        width : 80
      } ]
    });

    var config = {
      autoExpandColumn : (admin ? 'groupmanager' : 'members'),
      store : groupManagerStore,
      colModel : groupColModel,
      emptyText : this.emptygroupManagerStoreText,
      listeners : {
        fallBelowMinHeight : {
          fn : this.onFallBelowMinHeight,
          scope : this
        }
      }
    };

    Ext.apply(this, Ext.apply(this.initialConfig, config));
    Triology.groupmanager.Grid.superclass.initComponent.apply(this, arguments);

    // store grid in parent panel
    this.parentPanel.groupmanagerGrid = this;
  },

  onFallBelowMinHeight : function(height, minHeight) {
    var p = Ext.getCmp('groupmanagerPanel');
    this.setHeight(minHeight);
    var epHeight = p.getHeight();
    p.setHeight(epHeight - (minHeight - height));
    // rerender
    this.doLayout();
    p.doLayout();
    this.ownerCt.doLayout();
  },

  renderMembers : function(members) {
    var out = '';
    if (members) {
      var s = members.length;
      for ( var i = 0; i < s; i++) {
        out += members[i];
        if ((i + 1) < s) {
          out += ', ';
        }
      }
    }
    return out;
  },

  selectItem : function(group) {
    if (debug) {
      console.debug(group.name + ' selected');
    }

    if (this.parentPanel) {
      this.parentPanel.updateHistory(group);
    }

    if (admin) {
      Triology.groupmanager.setEditPanel([ {
        item : group,
        xtype : 'groupManagerForm',
        listeners : {
          updated : {
            fn : this.reload,
            scope : this
          },
          created : {
            fn : this.reload,
            scope : this
          }
        }
      }, {
        item : group,
        xtype : 'groupManagerMemberForm',
        listeners : {
          updated : {
            fn : this.reload,
            scope : this
          },
          created : {
            fn : this.reload,
            scope : this
          }
        }
      } ]);
    } else {
      Triology.groupmanager.setEditPanel([ {
        item : group,
        xtype : 'groupManagerMemberForm',
        listeners : {
          updated : {
            fn : this.reload,
            scope : this
          },
          created : {
            fn : this.reload,
            scope : this
          }
        }
      } ]);
    }
  }

});

// register xtype
Ext.reg('groupmanagerGrid', Triology.groupmanager.Grid);
