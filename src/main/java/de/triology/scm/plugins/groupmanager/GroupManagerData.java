/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * https://www.scm-manager.com
 * 
 */
package de.triology.scm.plugins.groupmanager;

import java.io.Serializable;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Holds the group managers for groups.
 * @author Jan Boerner, TRIOLOGY GmbH
 */
@XmlRootElement(name = "groupmanagers")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupManagerData implements Serializable {

  /**
   * Identifier used for serialization.
   */
  private static final long serialVersionUID = -3822899693739093766L;

  /** Keywords for auto close. */
  @XmlElement(name = "groupmanager")
  @XmlJavaTypeAdapter(XmlStringSetAdapter.class)
  private Set<String> groupmanager = null;

  /**
   * Default constructor used by JAXB.
   */
  public GroupManagerData() {

  }

  /**
   * @return the groupmanager
   */
  public final Set<String> getGroupmanager() {
    return groupmanager;
  }

  /**
   * Set the group managers.
   * @param manager The managers to set.
   */
  public final void setGroupmanager(final Set<String> manager) {
    groupmanager = manager;
  }

}
