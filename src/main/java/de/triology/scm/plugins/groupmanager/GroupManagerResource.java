/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * https://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.groupmanager;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.Manager;
import sonia.scm.group.Group;
import sonia.scm.group.GroupDAO;
import sonia.scm.group.GroupException;
import sonia.scm.group.GroupNotFoundException;
import sonia.scm.security.Role;
import sonia.scm.security.ScmSecurityException;
import sonia.scm.store.DataStore;
import sonia.scm.store.DataStoreFactory;
import sonia.scm.util.SecurityUtil;
import sonia.scm.web.security.AdministrationContext;
import sonia.scm.web.security.PrivilegedAction;

import com.google.common.base.Objects;
import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * GroupManager RESTful WebService endpoint.
 * This resource is available at /api/rest/groupmanager
 */
@Path("groupmanager")
@Singleton
public class GroupManagerResource {

  /** the logger for AbstractManagerResource */
  private static final Logger LOGGER = LoggerFactory.getLogger(GroupManagerResource.class);

  private static final String STORE_NAME = "groupmanager";

  /** Manager for groups. */
  private final Manager<Group, GroupException> manager;

  /** The group manager data store handler. */
  private final DataStore<GroupManagerData> managerDataStore;

  /** Access to the group store. */
  private final GroupDAO groupDAO;

  /** Admin-Context for administravie operations. */
  private final AdministrationContext adminContext;


  /**
   * Constructs a new GroupManagerResource and injects all required dependencies.
   * 
   * @param groupMgr The central class for managing groups.
   * @param dataStoreFactory The global factory for storing data.
   * @param admin AdministrationContext for privileged actions.
   */
  @Inject
  public GroupManagerResource(final DefaultGroupManager groupMgr, final GroupDAO groupDAO,
      final DataStoreFactory dataStoreFactory, final AdministrationContext admin) {
    manager = groupMgr;
    managerDataStore = dataStoreFactory.getStore(GroupManagerData.class, STORE_NAME);
    this.groupDAO = groupDAO;
    adminContext = admin;
  }

  /**
   * Creates an object that is prepared for the response.
   * 
   * @param items List of groups.
   * @return The groups with entities.
   */
  private GenericEntity<Collection<GroupWithManager>> createGenericEntity(final Collection<GroupWithManager> items) {
    return new GenericEntity<Collection<GroupWithManager>>(items) {
    };
  }

  /**
   * Returns all groups
   * 
   * @return The groups including group manager.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAll() {
    final Subject subject = SecurityUtils.getSubject();
    Response response = null;
    String username;
    Collection<GroupWithManager> groups = null;
    if (subject.hasRole(Role.ADMIN)) {
      groups = getAllGroups();
    } else if (subject.hasRole(Role.USER)) {
      username = (String) subject.getPrincipal();
      groups = getGroupsForGroupManager(username);
    }
    if (null != groups) {
      final Object entity = createGenericEntity(groups);
      response = Response.ok(entity).build();
      return response;
    }
    return Response.status(Status.FORBIDDEN).build();
  }

  /**
   * Modifies the given group.<br />
   * This method requires admin privileges.<br />
   * <br />
   * Status codes:
   * <ul>
   * <li>201 update successful</li>
   * <li>403 forbidden, the current user has no admin privileges</li>
   * <li>500 internal server error</li>
   * </ul>
   * 
   * @param uriInfo current uri informations
   * @param name name of the group to be modified
   * @param group group object to modify
   * @return
   */
  @PUT
  @Path("{id}")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response update(@Context final UriInfo uriInfo, @PathParam("id") final String name,
      final GroupWithManager group) {
    Response response = null;
    try {
      LOGGER.info("modify groupmanager of group {} of type {}", group.getName(), group.getType());

      SecurityUtil.assertIsAdmin();
      if (groupDAO.contains(name)) {
        final GroupManagerData data = new GroupManagerData();
        data.setGroupmanager(new HashSet<String>(group.getGroupmanager()));
        managerDataStore.put(name, data);
      } else {
        throw new GroupNotFoundException("group does not exists");
      }

      response = Response.noContent().build();
    } catch (final ScmSecurityException ex) {
      LOGGER.warn("delete not allowd", ex);
      response = Response.status(Response.Status.FORBIDDEN).build();
    } catch (final Exception ex) {
      LOGGER.error("error during update", ex);
      response = createErrorResonse(ex);
    }
    return response;
  }

  /**
   * Modify group members.
   * 
   * @param uriInfo Information about the request URI.
   * @param name Name of the group to modify.
   * @param group The given group object.
   * @return Status response of this operation.
   */
  @PUT
  @Path("group/{id}")
  @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Response updateGroupMembers(@Context final UriInfo uriInfo, @PathParam("id") final String name,
      final GroupWithManager group) {
    final List<Response> responses = new LinkedList<Response>();
    final Collection<String> managers = findManagerForGroup(name);
    if (null != managers && managers.contains(SecurityUtils.getSubject().getPrincipal())
        || SecurityUtils.getSubject().hasRole(Role.ADMIN)) {
      LOGGER.info("modify group {} of type {}", group.getName(), group.getType());
      adminContext.runAsAdmin(new PrivilegedAction() {

        @Override
        public void run() {
          try {
            final Group g = manager.get(name);
            g.setMembers(group.getMembers());
            g.setLastModified(new Date().getTime());
            manager.modify(g);
          } catch (final ScmSecurityException ex) {
            LOGGER.warn("delete not allowd", ex);
            responses.add(Response.status(Response.Status.FORBIDDEN).build());
          } catch (final Exception ex) {
            LOGGER.error("error during update", ex);
            responses.add(createErrorResonse(ex));
          }
        }
      });
    } else {
      LOGGER.warn("User {} is not permitted for managing group members of group {}.", SecurityUtils.getSubject()
          .getPrincipal(), group.getName());
      responses.add(Response.status(Response.Status.FORBIDDEN).build());
    }

    if (responses.size() > 0) {
      return responses.get(0);
    }
    return Response.noContent().build();
  }

  /**
   * Creates a response to inform the client about an error.
   * 
   * @param throwable Object containing informations about the error.
   * @return The error response object.
   */
  protected Response createErrorResonse(final Throwable throwable) {
    return Response
        .status(Status.INTERNAL_SERVER_ERROR)
        .entity(
            Objects.toStringHelper(this).add("message", throwable.getMessage())
            .add("stacktrace", Throwables.getStackTraceAsString(throwable))).build();
  }

  /**
   * Get all groups the given user belongs to.
   * 
   * @param member User name
   * @return All groups of this user.
   */
  private Collection<GroupWithManager> getGroupsForGroupManager(final String member) {
    final Collection<GroupWithManager> groups = new LinkedList<GroupWithManager>();
    final Collection<Group> allGroups = new LinkedList<Group>();
    adminContext.runAsAdmin(new PrivilegedAction() {

      @Override
      public void run() {
        allGroups.addAll(manager.getAll());

      }
    });
    for (final Group group : allGroups) {
      final Collection<String> mgrs = findManagerForGroup(group.getName());
      if (null != mgrs && mgrs.contains(member)) {
        final GroupWithManager gwm = new GroupWithManager(group);
        gwm.addManagers(mgrs);
        groups.add(gwm);
      }
    }

    return groups;
  }

  /**
   * Returns all groups.<br />
   * This method requires admin privileges.<br />
   * <br />
   * Status codes:
   * <ul>
   * <li>200 get successful</li>
   * <li>403 forbidden, the current user has no admin privileges</li>
   * <li>500 internal server error</li>
   * </ul>
   * 
   * @return all groups.
   */
  private Collection<GroupWithManager> getAllGroups() {
    final Collection<Group> items = manager.getAll();
    final Collection<GroupWithManager> gmItems = new LinkedList<GroupWithManager>();
    for (final Group g : items) {
      if (g.getId() != null) {
        final GroupWithManager gm = new GroupWithManager(g);
        final Collection<String> mg = findManagerForGroup(g.getName());
        gm.addManagers(mg);
        gmItems.add(gm);
      }
    }
    return gmItems;
  }

  /**
   * @param name
   */
  private Collection<String> findManagerForGroup(final String name) {
    Collection<String> result = new LinkedList<String>();
    final GroupManagerData data = managerDataStore.get(name);
    if (null != data) {
      result = data.getGroupmanager();
    }
    return result;
  }
}
